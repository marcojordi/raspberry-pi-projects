# -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
import time
import RPi.GPIO as GPIO 
###############################################################################
# Header
###############################################################################
__author__ = 'Benjamin Loeffel'
__maintainer__ = 'Benjamin Loeffel'
__email__ = 'benjamin.loeffel@gmx.ch'
###############################################################################
###############################################################################

class StepperMotor():
    """
    Steppermotor class to be used with a stepper motor driver
    """
    def __init__(self,PinDir,PinPulse,DelayTimeHigh=0.00005,DelayTimeLow=0.00035):
        """
        Initialization
        
        Parameters
        ----------
        PinDir : scalar, int,
            Pinname for direction-pin / BCM
            
        PinPulse : scalar, int,
            Pinname for pulse-pin / BCM
        
        DelayTimeHigh : scalar, float, optional
            PWM-Sigal hightime / seconds, default = 0.00005 s
            
        DelayTimeLow : scalar, float, optional
            PWM-Sigal lowtime / seconds, default = 0.0.00035 s
        """
        
        self.pindir=PinDir
        self.pinpulse=PinPulse
        self.DelayTimeHigh=DelayTimeHigh
        self.DelayTimeLow=DelayTimeLow
        GPIO.setup(self.pindir,GPIO.OUT,initial=GPIO.LOW)
        GPIO.setup(self.pinpulse,GPIO.OUT,initial=GPIO.LOW)
        print('Stepper is properly configured on PinDir {pindir} PinPulse {pinpulse}'.format(pindir=self.pindir,pinpulse=self.pinpulse))

    
    def Move(self,Direction,Steps):
        """
        Moves steppermotor in direction for amount of steps
        
        Parameters
        ----------
        Direction : scalar, int,
            Direction of rotation, 1 = forward, -1 = backward
            
        Steps : scalar, int,
            Steps
        """
           
        if Direction==1:
            GPIO.output(self.pindir,GPIO.HIGH)

        if Direction==-1:
            GPIO.output(self.pindir,GPIO.LOW)
            
        if Direction!=1 and Direction!=-1:
            raise NameError("Wrong direction flag. Values can be: 1 or -1")
            
        for Step in range(Steps):
            GPIO.output(self.pinpulse,GPIO.HIGH)
            time.sleep(self.DelayTimeHigh)
            GPIO.output(self.pinpulse,GPIO.LOW)
            time.sleep(self.DelayTimeLow)
