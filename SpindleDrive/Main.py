 # -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
import traceback
import time
from Components import StepperMotor
import RPi.GPIO as GPIO 
GPIO.setmode(GPIO.BCM)
###############################################################################
# Header
###############################################################################
__author__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']
###############################################################################
###############################################################################

if __name__ == "__main__":
    try:
        #Initialize Stepper Motor
        Stepper=StepperMotor(PinDir=6,PinPulse=5) 
        
        #Search Init position with Limit Switch on Pin 20
        print('Initialize Spindle Drive!')
        GPIO.setup(20,GPIO.IN)
        
        while True:
            input_value = GPIO.input(20)
            Stepper.Move(1,1) #Direction -1 = down, 1 = up
            
            
            if input_value == False:
                time.sleep(1)   
                Stepper.Move(-1,800)
                print('Init position is found')
                break
                    
    except:
        traceback.print_exc()   